export default {
    normal: {
        translatedName: 'normal',
        color: '#c4c084',
    },
    fighting: {
        translatedName: 'lutador',
        color: '#852816',
    },
    flying: {
        translatedName: 'voador',
        color: '#8fa4ff',
    },
    poison: {
        translatedName: 'venenoso',
        color: '#924990',
    },
    ground: {
        translatedName: 'solo',
        color: '#c5a455',
    },
    rock: {
        translatedName: 'pedra',
        color: '#5e491c',
    },
    bug: {
        translatedName: 'inseto',
        color: '#87950c',
    },
    ghost: {
        translatedName: 'fantasma',
        color: '#6969af',
    },
    steel: {
        translatedName: 'aço',
        color: '#7f8488',
    },
    fire: {
        translatedName: 'fogo',
        color: '#cf2c03',
    },
    water: {
        translatedName: 'água',
        color: '#3b9bf1',
    },
    grass: {
        translatedName: 'grama',
        color: '#67af32',
    },
    electric: {
        translatedName: 'elétrico',
        color: '#e08d00',
    },
    psychic: {
        translatedName: 'psíquico',
        color: '#e25484',
    },
    ice: {
        translatedName: 'gelo',
        color: '#84edf8',
    },
    dragon: {
        translatedName: 'dragão',
        color: '#7361d1',
    },
    dark: {
        translatedName: 'trevas',
        color: '#413831',
    },
    fairy: {
        translatedName: 'fada',
        color: '#e29fe6',
    },
    unknown: {
        translatedName: 'desconhecido',
        color: '#c4c084',
    },
    shadow: {
        translatedName: 'sombra',
        color: '#413831',
    },
};
