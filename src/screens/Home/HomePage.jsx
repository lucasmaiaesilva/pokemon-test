import React from 'react';
import pokemonLogo from '../../assets/images/pokemonLogo.png';
import Button from '../../components/Button';
import { useHistory } from "react-router-dom";

const HomePage = () => {
    let history = useHistory();

    function handleClick() {
        history.push("/map");
    }

    return (
        <div className="home">
            <img src={pokemonLogo} alt="logo" />
            <Button text="START" onClick={handleClick} />
        </div>
    );
};

export default HomePage;
