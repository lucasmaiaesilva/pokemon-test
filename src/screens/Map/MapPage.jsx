import React, { useState, useEffect } from 'react';
import serializeForm from 'form-serialize';
import Sidebar from '../../components/Sidebar';
import Character from '../../components/Character';
import Modal from '../../components/Modal';
import ModalContent from '../../components/ModalContent';

const MapPage = () => {
    const [ isModalOpened, setIsModalOpened ] = useState(false);
    const [ loading, setLoading ] = useState(false);
    const [ hasError, setHasError ] = useState(false);
    const [ over, setOver ] = useState(false);
    const [ fetchData, setFetchData ] = useState(false);
    const [ data, setData ] = useState({});
    const [ pokemonsSaved, setPokemonsSaved ] = useState([]);
    const [ isNewPokemon, setIsNewPokemon ] = useState(true);
    const [ newTemplate, setNewTemplate ] = useState(true);

    const submitForm = (e) => {
        e.preventDefault();
        const values = serializeForm(e.target, { hash: true });
        const returnStat = (name, value) => {
            return {
                stat: { name },
                base_stat: value,
            };
        }
        const data = {
            id: new Date().getTime(),
            name: values.name,
            sprites: {
                front_default: 'https://img.pokemondb.net/artwork/vector/large/unown-c.png',
            },
            // pass all types available on the api
            // https://pokeapi.co/api/v2/type/
            types: [],
            height: values.height * 100,
            weight: values.weight * 1000,
            stats:[
                returnStat('hp', values.hp),
                returnStat('attack', values.att),
                returnStat('defense', values.def),
                returnStat('special-attack', values.spatt),
                returnStat('special-defense', values.spdef),
                returnStat('speed', values.speed),
            ],
            abilities: [],
        };
        savePokemon(data);
        setIsModalOpened(false);
    };

    const savePokemon = (pokemon) => {
        if (pokemonsSaved.length === 8) {
            setIsModalOpened(false);
            setHasError(true);
        }
        else {
            setHasError(false);
            setPokemonsSaved(oldList => {
                return [
                    ...oldList,
                    pokemon,
                ];
            });
            setIsModalOpened(false);
        }
    }

    const deletePokemon = (id) => {
        setPokemonsSaved(oldState => {
            return oldState.filter(pokemon => pokemon.id !== id);
        });
        setIsModalOpened(false);
    }

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    const handleClickPokemon = async (index) => {
        await setNewTemplate(false);
        await setIsNewPokemon(false);
        setData(pokemonsSaved[index]);
        setIsModalOpened(true);
    };

    const handleClickNew = async () => {
        await setNewTemplate(true);
        setIsModalOpened(true);
    };

    useEffect(() => {
        if (fetchData) {
            setNewTemplate(false);
            setIsNewPokemon(true);
            const random = getRandomInt(1, 807);
            setLoading(true)
            fetch(`https://pokeapi.co/api/v2/pokemon/${random}`)
                .then(res => res.json())
                .then(res => {
                    setData(res);
                    setLoading(false);
                    setFetchData(false);
                })
        }
    }, [fetchData]);

    useEffect(() => {
        if (JSON.stringify(data) !== '{}') {
           setIsModalOpened(true);
        }
    }, [data]);

    return (
        <>
            <div className="map">
                <Sidebar
                    pokemonsList={pokemonsSaved}
                    handleClickPokemon={handleClickPokemon}
                    handleClickNew={handleClickNew}
                />
                <Character
                    isLoading={loading}
                    over={over}
                    hasError={hasError}
                    setOver={() => setOver(true)}
                    setLeave={() => setOver(false)}
                    handleClick={() => setFetchData(true)}
                />
                {isModalOpened && (
                    <Modal closeModal={() => setIsModalOpened(false)}>
                        {(JSON.stringify(data) !== '{}' || newTemplate) && (
                            <ModalContent
                                data={data}
                                onSavePokemon={savePokemon}
                                onDeletePokemon={deletePokemon}
                                isNewPokemon={isNewPokemon}
                                type={newTemplate ? 'new': 'list'}
                                submitNewForm={newTemplate ? submitForm : null}
                            />
                        )}
                    </Modal>
                )}
            </div>
        </>
    );
};

export default MapPage;
