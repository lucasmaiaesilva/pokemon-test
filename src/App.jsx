import React from 'react';
import { Route, Switch } from 'react-router-dom';
import MapPage from './screens/Map/MapPage';
import HomePage from './screens/Home/HomePage';

const App = () => (
    <Switch>
        <Route path="/map" component={MapPage} />
        <Route path="/" component={HomePage} />
    </Switch>
);

export default App;
