import React from 'react';
import Button from './Button';
import plusIcon from '../assets/images/plus.png';

const Sidebar = ({
    pokemonsList,
    handleClickPokemon,
    handleClickNew,
}) => {
    return (
        <div className="sidebar">
            {pokemonsList.map((pokemon, index) => (
                <div
                    className="sidebar__item"
                    key={index}
                    onClick={() => handleClickPokemon(index)}
                >
                    <img src={pokemon.sprites.front_default} alt="thumb" />
                </div>
            ))}
            <Button
                icon={<img src={plusIcon} alt="+" />}
                onClick={handleClickNew}
            />
        </div>
    );
};

export default Sidebar;
