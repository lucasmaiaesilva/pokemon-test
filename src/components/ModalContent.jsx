import React from 'react';
import StatisticBlock from './StatisticBlock';
import Button from './Button';
import pokeball from '../assets/images/pokeball.png';
import camera from '../assets/images/camera.png';
import transformType from '../utils/transformType';
import TextInput from './TextInput';
import NumberInput from './NumberInput';
import Dropdown from './Dropdown';


const ModalContent = ({
    data,
    onSavePokemon,
    isNewPokemon,
    onDeletePokemon,
    type,
    submitNewForm = () => {},
}) => {
    const {
        id,
        name,
        sprites,
        height,
        weight,
        types,
        abilities,
        stats,
    } = data;

    const NewForm = () => {
        return (
            <form onSubmit={submitNewForm}>
                <TextInput
                    label="Nome"
                    placeholder="Nome"
                    name="name"
                    required
                />
                <NumberInput label="HP" placeholder="HP" name="hp" required />
                <NumberInput
                    label="PESO"
                    placeholder="PESO"
                    name="weight"
                    suffix="kg"
                    required
                />
                <NumberInput
                    label="ALTURA"
                    placeholder="ALTURA"
                    name="height"
                    suffix="cm"
                    required
                />
                <StatisticBlock title="tipo">
                    <Dropdown options={[]} />
                </StatisticBlock>
                <StatisticBlock title="habilidades">
                    <TextInput placeholder="habilidade1" name="skill1" />
                    <TextInput placeholder="habilidade2" name="skill2" />
                    <TextInput placeholder="habilidade3" name="skill3" />
                    <TextInput placeholder="habilidade4" name="skill4" />
                </StatisticBlock>

                <StatisticBlock title="estatísticas">
                    <NumberInput
                        label="DEFESA"
                        placeholder="00"
                        name="def"
                        required
                    />
                    <NumberInput
                        label="ATAQUE"
                        placeholder="00"
                        name="att"
                        required
                    />
                    <NumberInput
                        label="DEFESA ESPECIAL"
                        placeholder="00"
                        name="spdef"
                        required
                    />
                    <NumberInput
                        label="ATAQUE ESPECIAL"
                        placeholder="00"
                        name="spatt"
                        required
                    />
                    <NumberInput
                        label="VELOCIDADE"
                        placeholder="00"
                        name="speed"
                        required
                    />
                </StatisticBlock>

                <Button className="submit" text="CRIAR POKEMON" type="submit" />
            </form>
        );
    };


    const ListContent = () => (
        <>
            <div className="title">
                {name}
            </div>
            <div className="display">
                <div>
                    <div className="skill-title">
                        hp
                    </div>
                    45/45
                </div>
                <div>
                    <div className="skill-title">
                        altura
                    </div>
                    {`${height / 100} m`}
                </div>
                <div>
                    <div className="skill-title">
                        peso
                    </div>
                    {`${weight / 1000} kg`}
                </div>
            </div>
            <StatisticBlock title="tipo">
                <div className="labels-block">
                    {types.map(item => (
                        <Button
                            key={item.type.name}
                            text={transformType[item.type.name].translatedName || item.type.name}
                            className="label"
                            style={{
                                background: transformType[item.type.name].color,
                                border: 'none'
                            }}
                        />
                    ))}
                </div>
            </StatisticBlock>

            <StatisticBlock title="habilidades">
                <div className="skills-block">
                    {abilities.map(item => (
                        <span key={item.ability.name}>
                            {`${item.ability.name}, `}
                        </span>
                    ))}
                </div>
            </StatisticBlock>

            <StatisticBlock title="estatísticas">
                <div className="statistics-box">
                    {stats.map((item, index) => (
                        <div className="item" key={index}>
                            <div>{item.stat.name}</div>
                            <div>{item.base_stat}</div>
                        </div>
                    ))}
                </div>
            </StatisticBlock>
            {isNewPokemon ? (
                <img
                    className="submit"
                    src={pokeball}
                    alt="pokeball"
                    onClick={() => onSavePokemon({
                        id: new Date().getTime(),
                        name,
                        sprites,
                        height,
                        weight,
                        types,
                        abilities,
                        stats,
                    })}
                />
            ): (
                <Button
                    className="submit"
                    text="LIBERAR POKEMON"
                    onClick={() => onDeletePokemon(id)}
                />
            )}
        </>
    );

    return (
        <div className="modalbox">
            <div className="modalbox__header">
                <div className="thumb-pokemon">
                    {type === 'new' ? (
                        <img src={camera} alt="pokemon thumb" />
                    ): (
                        <img src={sprites.front_default} alt="pokemon thumb" />
                    )}
                </div>
            </div>
            <div className="modalbox__content">
                {type === 'new' ? (
                    <NewForm />
                ): (
                    <ListContent />
                )}
            </div>
        </div>
    )
}

export default ModalContent;
