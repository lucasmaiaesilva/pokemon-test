import React from 'react';
import PropTypes from 'prop-types';
import ashFront from '../assets/images/ashFront.png';
import search from '../assets/images/searchingTooltip.png';
import load from '../assets/images/searchTooltip.png';
import error from '../assets/images/tooltipError.png';

const Character = ({
    isLoading = false,
    hasError = false,
    over = false,
    setOver,
    setLeave,
    handleClick,
}) => {
    return (
        <div className="character">
            <div className="character__box">
                <img
                    onMouseOver={setOver}
                    onMouseLeave={setLeave}
                    onClick={handleClick}
                    src={ashFront}
                    alt="character"
                />
                {isLoading && (
                    <img className="tooltip" src={search} alt="search-tootltip" />
                )}
                {hasError && (
                    <img className="tooltip" src={error} alt="error-tootltip" />
                )}
                {over && !isLoading && (
                    <img className="tooltip" src={load} alt="search-tootltip" />
                )}
            </div>
        </div>
    )
}

Character.propTypes = {
    isLoading: PropTypes.bool,
    hasError: PropTypes.bool,
};

export default Character;
