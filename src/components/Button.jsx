import React from 'react';

const Button = ({
    text,
    icon,
    onClick,
    className,
    type,
    style = {},
}) => {

    return (
        <button
            className={`btn btn--${text ? 'text' : 'icon'} ${className || ''}`}
            style={style}
            onClick={onClick}
            type={type}
        >
            {text || icon}
        </button>
    )
};

export default Button;
