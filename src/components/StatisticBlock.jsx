import React from 'react';

const Title = ({ children }) => (
    <div className="subtype">
        <div className="subtype__title">{children}</div>
    </div>
);

const StatisticBlock = ({
    title,
    children
}) => {
    return (
        <>
            <Title>{title}</Title>
            {children}
        </>
    )
}

export default StatisticBlock;
